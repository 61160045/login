import { AUTH_LOGIN, AUTH_LOGOUT } from '../mutation-types'
import router from '../../router'
export default {
  namespaced: true,
  state: () => ({
    user: null
  }),
  getters: {
    isLogin (state, getters) {
      return state.user != null
    }
  },
  mutations: {
    [AUTH_LOGIN] (state, payload) { // payload เป็นกล่องที่ใส่ของเข้ามา
      state.user = payload
    },
    [AUTH_LOGOUT] (state) {
      state.user = null
    }
  },
  actions: {
    login ({ commit }, payload) { // commit ดึง object ของ context มา
      console.log(payload)
      // if correct
      const user = { name: 'Administrator', email: 'admin@user.com' }
      router.push('/')
      commit(AUTH_LOGIN, user)
    },
    logout ({ commit }) {
      commit(AUTH_LOGOUT)
    }
  }
}
